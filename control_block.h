#include <iostream>
class ctrl_block{
    public:
    ctrl_block():_count(0){}
    ~ctrl_block(){}

    ctrl_block(const ctrl_block&)=delete;
    ctrl_block& operator=(const ctrl_block&)=delete;
     public:
    int get(){return _count;}
    void reset(){_count=0;}

    /*operators*/

    void operator++(){_count++;}
    void operator++ (int){
        ++_count;
        }

    void operator--(){_count--;}
    void operator-- (int){
        --_count;
        }
  
    private:
    unsigned int _count{};

};

  std::ostream& operator<<(std::ostream& os,ctrl_block& ctrl){
        os << "Counter Value : " << ctrl.get()<< std::endl;
           
        return os;

    }