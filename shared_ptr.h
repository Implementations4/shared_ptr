#include <iostream>
#include <control_block.h>

template<typename T>
class shared_ptr
{
private:
T* _shr_ptr;
ctrl_block* ctrl_ptr;
    
public:
    shared_ptr(T* ptr){
        _shr_ptr=ptr;
        ctrl_ptr=new ctrl_block();
        (*ctrl_ptr)++;


    }
    shared_ptr(shared_ptr<T>& other){
        _shr_ptr=other._shr_ptr;
         ctrl_ptr = other.ctrl_ptr;
        (*ctrl_ptr)++;

    }

    shared_ptr& operator=(shared_ptr<T>& other){
        _shr_ptr=other._shr_ptr;
         ctrl_ptr = other.ctrl_ptr;
        (*ctrl_ptr)++;
    }


    ~shared_ptr(){
        (*ctrl_ptr)--;
        if(ctrl_ptr->get()==0){
            delete _shr_ptr;
            delete ctrl_ptr;
        }

    }

    // Reference count
    unsigned int use_count()
    {
      return ctrl_ptr->get();
    }
 
    // Get the pointer
    T* get()
    {
      return _shr_ptr;
    }
 
    // Overload * operator
    T& operator*()
    {
      return *_shr_ptr;
    }
 
    // Overload -> operator
    T* operator->()
    {
      return _shr_ptr;
    }
   
      friend std::ostream& operator<<(std::ostream& os,
                               shared_ptr<T>& sp)
    {
        os << "Address pointed : " << sp.get() << std::endl;
        os << *(sp.ctrl_ptr) << std::endl;
        return os;
    }




};


